<?php

if (isset($_POST['save'])) {
    $name = mysql_real_escape_string(trim($_POST['name']));
    if ($_POST['time_end'] == 1 || ($_POST['time_end'] != 1 && $_POST['time_end'] != 2 && $_POST['time_end'] != 3 && $_POST['time_end'] != 4 && $_POST['time_end'] != 5 && $_POST['time_end'] != 6)) {
        $time_end = 0;
    } elseif ($_POST['time_end'] == 1) {
        $time_end = 0;
    } elseif ($_POST['time_end'] == 2) {
        $time_end = ($vote->time_end < time()) ? $vote->time_end+time()+60*60*24 : time()+60*60*24;
    } elseif ($_POST['time_end'] == 3) {
        $time_end = ($vote->time_end < time()) ? $vote->time_end+time()+60*60*72 : time()+60*60*72;
    } elseif ($_POST['time_end'] == 4) {
        $time_end = ($vote->time_end < time()) ? $vote->time_end+time()+60*60*24*7 : time()+60*60*24*7;
    } elseif ($_POST['time_end'] == 5) {
        $time_end = ($vote->time_end < time()) ? $vote->time_end+time()+60*60*24*30 : time()+60*60*24*30;
    } elseif ($_POST['time_end'] == 6) {
        $time_end = ($vote->time_end < time()) ? $vote->time_end+time()+60*60*24*90 : time()+60*60*24*90;
    }
    if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\:\,\.\-)]/", $_POST['name'], $m)) {
    ?>
<div class = 'err'>
    В поле &laquo;Название опроса&raquo; присутствуют запрещенные <span style="font-weight: bold; color: red;"><?= $m[0]?></span> символы!.
</div>
            <?            
        } else 
    if (mb_strlen($name) < 5) {
        ?>
        <div class = 'err'>Слишком короткое содержание опроса.</div>
        <?
    } else {
        unset($_SESSION['name']);
        unset($_SESSION['time_end']);
        $var = $_POST['var'];
        $count_var = count($var); 
        $check = 0;
        for ($i = 0; $i < $count_var; $i++) {
            unset($_SESSION['var'][$i]);
            $id = mysql_fetch_object(mysql_query('SELECT `id` FROM `forum_votes_var` WHERE `id_vote` = '.$vote->id.' AND `variant` = "'.$var[$i].'"'));
            $check += preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\:\,\.\-)]/", $var[$i]);            
            $val[] = "('".($id ? $id->id : NULL)."', '".$theme->id."', '".$var[$i]."', '".$vote->id . "')";
            mysql_query('DELETE FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id.' AND `variant` = ""');
        }
        if ($check == false) {
        $sql = 'INSERT INTO `forum_votes_var` (`id`, `id_theme`, `variant`, `id_vote`)
        VALUES ' . join(',', $val) . ' 
        ON DUPLICATE KEY UPDATE 
        `variant`=VALUES(`variant`)';
        mysql_query($sql);
        $_SESSION['success'] = '<div class = "msg">Опрос успешно изменён.</div>';
        mysql_query('UPDATE `forum_votes` SET  `name` = "'.$name.'", `time_end` = '.$time_end.' WHERE `id_theme` = '.$theme->id);
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
        } else {
        echo '<div class = "err">Ошибка в описании вариантов!</div>';        
        }
    }
} elseif (isset($_POST['add_var']) && $vars < 9) {
    $_SESSION['name'] = $_POST['name'];
    $_SESSION['time_end'] = $_POST['time_end'];
    $var = $_POST['var'];
    $count_var = count($var);
    for ($i = 0; $i < $count_var; $i++) {
        $_SESSION['var'][$i] = mysql_real_escape_string(trim($var[$i]));
    }
    mysql_query('INSERT INTO `forum_votes_var` SET `id_theme` = '.$theme->id.', `id_vote` = '.$vote->id.', `variant` = ""');
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/edit_vote.html');
    exit;
} elseif (isset($_POST['delete_var']) && $vars > 2) {
    $_SESSION['name'] = $_POST['name'];
    $_SESSION['time_end'] = $_POST['time_end'];
    $var = $_POST['var'];
    $count_var = count($var)-1;
    unset($_SESSION['var'][$count_var]);
    $variant = mysql_result(mysql_query('SELECT MAX(`id`) FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id), 0);
    mysql_query('DELETE FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id.' AND `id` = '.$variant);
    mysql_query('DELETE FROM `forum_vote_voices` WHERE `id_variant` = '.$variant);
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/edit_vote.html');
    exit;
} elseif (isset($_POST['cancel']) && ($theme->type == 0 || ($theme->type == 1 && user_access('forum_post_close')))) {
    $_SESSION['success'] = '<div class = "msg">Редактирование успешно отменено.</div>';
    $var = $_POST['var'];
    $count_var = count($var);
    for ($i = 0; $i < $count_var; $i++) {
        unset($_SESSION['var'][$i]);
    }
    unset($_SESSION['name']);
    unset($_SESSION['time_end']);
    mysql_query('DELETE FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id.' AND `variant` = ""');
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
    exit;
} elseif (isset($_POST['delete'])) {
    $_SESSION['success'] = '<div class = "msg">Голосование успешно удалено.</div>';
    $var = $_POST['var'];
    $count_var = count($var);
    for ($i = 0; $i < $count_var; $i++) {
        unset($_SESSION['var'][$i]);
    }
    unset($_SESSION['name']);
    unset($_SESSION['time_end']);
    mysql_query('DELETE FROM `forum_vote_voices` WHERE `id_vote` = '.$vote->id);
    mysql_query('DELETE FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id);
    mysql_query('DELETE FROM `forum_votes` WHERE `id_theme` = '.$theme->id);
    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
    exit;
}
?>
<div class = 'menu_razd'>Редактирование опроса в теме</div>
<form action = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/edit_vote.html' method = 'post' class="p_m">
    <b>Содержание опроса:</b><br />
    <textarea name = 'name' style = 'width: 96%'><?= (isset($_SESSION['name'])) ? output_text($_SESSION['name']) : output_text($vote->name) ?></textarea><br /><br />
    <b>Варианты ответов:</b><br />
    <?
    $i = 0;
    $all_vars = mysql_query('SELECT `variant` FROM `forum_votes_var` WHERE `id_vote` = '.$vote->id.' ORDER BY `id` ASC');
    while ($var = mysql_fetch_object($all_vars)) {
        ?>
        <input type = 'text' name = 'var[]' value = '<?= (isset($_SESSION['var'][$i])) ? output_text($_SESSION['var'][$i]) : output_text($var->variant) ?>' style = 'width: 96%'><br />
        <?
        $i++;
    }
    ?>
    <input type = 'submit' name = 'add_var' value = 'Ещё вариант' <?= ($vars > 7) ? 'disabled = "disabled"' : NULL ?> /> <input type = 'submit' name = 'delete_var' value = 'Убрать вариант' <?= ($vars < 3) ? 'disabled = "disabled"' : NULL ?> /><br />
    <br />
    <b><?= ($vote->time_end < time()) ? 'Продлить на:' : 'Дата окончания через:' ?></b><br />
    <select name = 'time_end'>
        <?
        if ($vote->time_end < time()) {
            ?>
            <option value = "0">Без изменений</option>
            <?
        }
        ?>
        <option value = "1" <?= (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 1) ? 'selected = "selected"' : NULL ?>>Бессрочно</option>
        <option value = "2" <?= (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 2) ? 'selected = "selected"' : NULL ?>>1 День</option>
        <option value = "3" <?= (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 3) ? 'selected = "selected"' : NULL ?>>3 Дня</option>
        <option value = "4" <?= (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 4) ? 'selected = "selected"' : NULL ?>>1 Неделю</option>
        <option value = "5" <?= (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 5) ? 'selected = "selected"' : NULL ?>>1 месяц</option>
        <option value = "6" <?= (isset($_SESSION['time_end']) && $_SESSION['time_end'] == 6) ? 'selected = "selected"' : NULL ?>>3 месяца</option>
    </select><br />
    <input type = 'submit' name = 'save' value = 'Сохранить' /> <input type = 'submit' name = 'delete' value = 'Удалить' /> <input type = 'submit' name = 'cancel' value = 'Отменить' /><br />
</form>
<?

include_once '../sys/inc/tfoot.php';
exit;

?>